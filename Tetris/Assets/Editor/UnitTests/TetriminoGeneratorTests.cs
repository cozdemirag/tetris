﻿using Assets.SCRIPTS.Interfaces;
using Assets.SCRIPTS.Utility;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Editor.UnitTests
{
    [TestFixture]
    public class TetriminoGeneratorTests
    {
        private ITetriminoGenerator sut;
        private int maxIndex = 7;
        private int highBoundary = 6;

        private Mock<IRandomNumberProvider> mockRandomNumberProvider;


        [SetUp]
        public void SetUp()
        {
            this.mockRandomNumberProvider = new Mock<IRandomNumberProvider>();
            this.sut = new TetriminoGenerator(maxIndex, this.mockRandomNumberProvider.Object);
        }


        [Test]
        public void GivenSameTetriminoIsReturnedTwiceInARow_WhenGetTetriminoIsCalled_ThenTetriminosAreReturned()
        {
            // Arrange
            this.mockRandomNumberProvider.SetupSequence(it => it.GetNext(0, this.highBoundary))
                .Returns(1)
                .Returns(1);

            var expected = Tetriminos.T;

            // Act
            var actual1 = this.sut.GetTetriminoIndex();
            var actual2 = this.sut.GetTetriminoIndex();

            // Assert
            Assert.AreEqual(expected, actual1);
            Assert.AreEqual(expected, actual2);
        }

        [Test]
        public void GivenSameTetriminoReturnedThriceInARow_WHenGetTetriminoIsCalled_Then3rdTetriminoWillBeReTried()
        {
            // Arrange
            this.mockRandomNumberProvider.SetupSequence(it => it.GetNext(0, this.highBoundary))
                .Returns(1)
                .Returns(1)
                .Returns(1)
                .Returns(3);

            var expected = Tetriminos.T;
            var expected_3rd = Tetriminos.O;

            // Act
            var actual1 = this.sut.GetTetriminoIndex();
            var actual2 = this.sut.GetTetriminoIndex();
            var actual3 = this.sut.GetTetriminoIndex();


            // Assert
            Assert.AreEqual(expected, actual1);
            Assert.AreEqual(expected, actual2);
            Assert.AreEqual(expected_3rd, actual3);
            this.mockRandomNumberProvider.Verify(it => it.GetNext(0, this.highBoundary), Times.Exactly(4));
        }

        [Test]
        public void GivenNoITetriminoIsReturnedFor12Times_WHenGetTetriminoIsCalled_ThenITetriminoIsReturnedFor13th()
        {
            // Arrange
            this.mockRandomNumberProvider.SetupSequence(it => it.GetNext(0, this.highBoundary))
                .Returns(0).Returns(0) // Z
                .Returns(1).Returns(1) // T
                .Returns(2).Returns(2) // S
                .Returns(3).Returns(3) // O
                .Returns(4).Returns(4) // L
                .Returns(0).Returns(0) // Z
                .Returns(1).Returns(1); // T
               
            var actuals = new List<Tetriminos>();

            try
            {
                // Act
                for (var i = 0; i < 13; i++)
                {
                    actuals.Add(this.sut.GetTetriminoIndex());
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

            // Assert
            Assert.AreEqual(Tetriminos.Z, actuals[0]);
            Assert.AreEqual(Tetriminos.Z, actuals[1]);
            Assert.AreEqual(Tetriminos.T, actuals[2]);
            Assert.AreEqual(Tetriminos.T, actuals[3]);
            Assert.AreEqual(Tetriminos.S, actuals[4]);
            Assert.AreEqual(Tetriminos.S, actuals[5]);
            Assert.AreEqual(Tetriminos.O, actuals[6]);
            Assert.AreEqual(Tetriminos.O, actuals[7]);
            Assert.AreEqual(Tetriminos.L, actuals[8]);
            Assert.AreEqual(Tetriminos.L, actuals[9]);
            Assert.AreEqual(Tetriminos.Z, actuals[10]);
            Assert.AreEqual(Tetriminos.Z, actuals[11]);
            Assert.AreEqual(Tetriminos.I, actuals[12]);
        }

        [Test]
        public void GivenRandomGeneratorReturnsInvalidTetriminosForFirst4_WhenGetTetriminoIsCalled_ThenItRetiries3TimesToReturnAValidStartingTetrimino()
        {
            // Arrange
            this.mockRandomNumberProvider.SetupSequence(it => it.GetNext(0, this.highBoundary))
                .Returns(0).Returns(2).Returns(1)                   // -1st Z S T
                .Returns(2).Returns(4)                              // -2nd S L 
                .Returns(5)                                         // -3rd O
                .Returns(0).Returns(2).Returns(2).Returns(0)        // -4th Z S S Z -> must default to O-Tetrimino
                .Returns(2)                                         // S        
                .Returns(0);                                        // Z

            var actuals = new List<Tetriminos>();

            try
            {
                // Act
                for (var i = 0; i < 6; i++)
                {
                    actuals.Add(this.sut.GetTetriminoIndex());
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

            // Assert
            Assert.AreEqual(Tetriminos.T, actuals[0]);
            Assert.AreEqual(Tetriminos.L, actuals[1]);
            Assert.AreEqual(Tetriminos.O, actuals[2]);
            Assert.AreEqual(Tetriminos.O, actuals[3]);
            Assert.AreEqual(Tetriminos.S, actuals[4]);
            Assert.AreEqual(Tetriminos.Z, actuals[5]);

        }
    }
}
