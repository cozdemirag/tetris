﻿using Assets.SCRIPTS.Interfaces;
using Assets.SCRIPTS.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Assets.SCRIPTS
{
    public class DefaultTetriminoGenerator : ITetriminoGenerator
    {
        private readonly int maxGapBetweenIMino;
        private readonly int maxRepeatSame;
        private readonly int max;

        private readonly Queue<Tetriminos> queue;
        private int queueRenewCount;


        public DefaultTetriminoGenerator()
        {
            var values = Enum.GetValues(typeof(Tetriminos)).Cast<int>().ToList();
            this.max = values.Count;

            this.queue = this.GetInitialQueue();
        }

        public int? GetIDraughtCountdown()
        {
            throw new NotImplementedException();
        }

        public Tetriminos GetTetriminoIndex()
        {
            while (true)
            {

                return Tetriminos.J;
            }
        }

        private Queue<Tetriminos> GetInitialQueue()
        {
            var tetrimino = Tetriminos.Z;
            var index = 0;
            do
            {
                index = UnityEngine.Random.Range(0, this.max - 1);
            } while (index >= 0 && index < this.max );


            return new Queue<Tetriminos>();
        }
    }
}
