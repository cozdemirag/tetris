﻿using Assets.Scripts.ViewControllers;
using Assets.SCRIPTS.Models;
using Assets.SCRIPTS.Repositories;
using Assets.SCRIPTS.Utility;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectProfileSceneViewController : MonoBehaviour
{

    public GameObject ContentContainer;
    public ToggleGroup ToggleGroup;

    public Toggle ProfilePrefab;

    private ProfileRepository profileRepository;
    private Dictionary<string, Sprite> profileImages;

    // Use this for initialization
    void Start()
    {
        this.profileImages = new Dictionary<string, Sprite>();
        this.profileRepository = new ProfileRepository();
        this.LoadProfiles();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LoadProfiles()
    {
        var profiles = this.profileRepository.GetPlayers();

        for (var index = 0; index < profiles.Count; index++)
        {
            var info = profiles.ElementAt(index);
            var option = Instantiate(this.ProfilePrefab, this.ContentContainer.transform);
            var toggle = option.GetComponent<Toggle>();
            toggle.group = this.ToggleGroup;

            if (index == 0)
            {
                EventSystem.current.firstSelectedGameObject = option.gameObject;
            }

            var textObjs = option.GetComponentsInChildren<Text>(true);
            var idObj = textObjs.FirstOrDefault(it => it.name == "ID");
            var nameObj = textObjs.FirstOrDefault(it => it.name == "Name");

            var background = option.transform.GetComponentsInChildren<Image>().FirstOrDefault(it => it.name == "Background");

            if (idObj == null || nameObj == null || background == null)
            {
                Debug.Log("in sufficient UI obj found");
                continue;
            }

            idObj.text = info.Id.ToString();
            nameObj.text = info.Name;


            Sprite image;

            if (!this.profileImages.TryGetValue(info.ImageName, out image))
            {
                var t = Resources.Load<Texture2D>(info.ImageName);
                if (t != null)
                {
                    var img = Sprite.Create(t, new Rect(0, 0, t.width, t.height), new Vector2(t.width / 2, t.height / 2));
                    this.profileImages.Add(info.ImageName, img);
                    image = img;
                }
            }

            if (image != null)
            {
                background.sprite = image;
            }
        }
    }


    public void SelectProfileButton_OnClick()
    {
        var activeToggle = this.ToggleGroup.ActiveToggles().FirstOrDefault();

        if (activeToggle == null)
        {
            return;
        }

        var idObj = activeToggle.GetComponentsInChildren<Text>(true).FirstOrDefault(it => it.name == "ID");

        if(idObj == null)
        {
            return;
        }

        var found = this.profileRepository.GetPlayers().FirstOrDefault(it => it.Id.ToString().Equals(idObj.text));

        if (found == null)
        {
            return;
        }
        this.profileRepository.SetProfileAsMain(found.Id);
        Sprite image = null;
        this.profileImages.TryGetValue(found.ImageName, out image);

        ProfileInfoSelectionProvider.MainProfile = new ProfileInfo
        {
            ID = found.Id,
            Image = image,
            Name = found.Name
        };

        if (!string.IsNullOrEmpty(SceneHandler.GetInstance().PreviousSceneName))
        {
            SceneHandler.GetInstance().LoadPreviousScene();
        }
        else
        {
            SceneHandler.GetInstance().LoadMainMenu();
        }
    }

    public void BackButton_OnClick()
    {
        if (!string.IsNullOrEmpty(SceneHandler.GetInstance().PreviousSceneName))
        {
            SceneHandler.GetInstance().LoadPreviousScene();
        }
        else
        {
            SceneHandler.GetInstance().LoadMainMenu();
        }
    }
}
