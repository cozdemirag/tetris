﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.ViewControllers
{
    public sealed class SceneHandler
    {
        public string PreviousSceneName;


        private static SceneHandler _Instance;

        public static SceneHandler GetInstance()
        {
            if(_Instance == null)
            {
                _Instance = new SceneHandler();
                return _Instance;
            }

            return _Instance;
        }

        private SceneHandler()
        {
        }

        public void StartP1Game()
        {
            SceneManager.LoadScene("game_p1", LoadSceneMode.Single);
        }

        public void StartP2Game()
        {
            SceneManager.LoadScene("game_p2", LoadSceneMode.Single);
        }

        public void LoadMainMenu()
        {
            SceneManager.LoadScene("menu", LoadSceneMode.Single);
        }

        public void LoadSettings()
        {
            SceneManager.LoadScene("settings", LoadSceneMode.Single);
        }

        public void LoadAbout()
        {
            SceneManager.LoadScene("about", LoadSceneMode.Single);
        }

        public void LoadScoreboard()
        {
            SceneManager.LoadScene("scoreboard", LoadSceneMode.Single);
        }

        public void LoadP2ProfileSelection()
        {
            SceneManager.LoadScene("p2_profile_selection", LoadSceneMode.Single);
        }

        public void ApplicationQuit()
        {
            Application.Quit();
        }

        public void LoadNewProfileScene()
        {
            this.PreviousSceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene("new_profile", LoadSceneMode.Single);
        }

        public void LoadChangeProfileScene()
        {
            SceneManager.LoadScene("select_a_profile", LoadSceneMode.Single);
        }

        public void LoadPreviousScene()
        {
            if (string.IsNullOrEmpty(this.PreviousSceneName))
            {
                return;
            }
            var scenename = this.PreviousSceneName;
            this.PreviousSceneName = null;
            SceneManager.LoadScene(scenename);
        }

        //public IEnumerator UnloadSettingsScene()
        //{
        //    if (this.previousSceneIndex >= 0)
        //    {
        //        var async = SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("settings"));

        //      //  ToggleEventSystemInActiveScene();

        //        yield return null;

        //        while (!async.isDone)
        //        {
        //            yield return null;
        //        }

        //    }
        //}

        //private void ToggleEventSystemInActiveScene()
        //{
        //    var activeScene = SceneManager.GetActiveScene();
        //    var eventSystems = activeScene.GetRootGameObjects().Where(it => it.name == "EventSystem").ToList();
        //    foreach(var eventSystem in eventSystems)
        //    {
        //        eventSystem.SetActive(!eventSystem.activeSelf);
        //    }
        //}
    }
}
