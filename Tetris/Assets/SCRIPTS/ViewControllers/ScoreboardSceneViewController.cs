﻿using Assets.Scripts.ViewControllers;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.SCRIPTS.ViewControllers
{
    public class ScoreboardSceneViewController : MonoBehaviour
    {
        public GameObject ContentContainer;

        private ScoreboardRepository scoreRepository;

        public void Awake()
        {
            this.scoreRepository = new ScoreboardRepository();
        }

        public void Start()
        {
            this.SetupScoreboardItems();
        }

        public void BackButton_OnClick()
        {
            SceneHandler.GetInstance().LoadMainMenu();
        }

        private void SetupScoreboardItems()
        {
            var receivedScores = this.scoreRepository.GetTop10P1Scores();

            for (var i = 0; i < receivedScores.Count; i++)
            {
                var scoreItem = this.ContentContainer.transform.GetChild(i);
                var textElements = scoreItem.GetComponentsInChildren<Text>();
                var scoreText = textElements.FirstOrDefault(it => it.name == "ScoreText");
                var lineText = textElements.FirstOrDefault(it => it.name == "LineNumberText");
                var nameText = textElements.FirstOrDefault(it => it.name == "NameText");

                var score = receivedScores.ElementAt(i);
                lineText.text = string.Format("{0}.", i + 1);
                nameText.text = score.PlayerName;
                scoreText.text = score.Score.ToString();
            }
        }
    }
}
