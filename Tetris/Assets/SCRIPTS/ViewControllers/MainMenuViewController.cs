﻿using Assets.Scripts.ViewControllers;
using Assets.SCRIPTS.Models;
using Assets.SCRIPTS.Repositories;
using Assets.SCRIPTS.Utility;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuViewController : MonoBehaviour
{
    public Toggle MusicToggleButton;
    public Text ProfileNameText;
    public Image ProfileImage;

    private ProfileRepository profileRepository;

    void Start()
    {
        this.profileRepository = new ProfileRepository();
        ProfileInfoSelectionProvider.Reset();

        StartCoroutine(this.MusicToggleButton.SetDefaultToggleState_CoRoutine(!SoundManager.GetInstance().IsBackgroundMusicMuted()));

        var allProfiles = this.profileRepository.GetPlayers();

        if (allProfiles.Count <= 0)
        {
            Debug.Log("redirecting to new profile scene");
            SceneHandler.GetInstance().LoadNewProfileScene();
            return;
        }

        var profileAmbiguityFound = allProfiles.Count(it => it.IsMain == true) > 1 || allProfiles.Count(it => it.IsMain == true) <= 0;
        if (profileAmbiguityFound)
        {
            Debug.Log("ambiguity found - redirecting to select profile scene");
            SceneHandler.GetInstance().LoadChangeProfileScene();
            return;
        }

        var foundProfile = allProfiles
            .FirstOrDefault(it => 
                (ProfileInfoSelectionProvider.MainProfile != null && it.Id == ProfileInfoSelectionProvider.MainProfile.ID) 
                || it.IsMain == true
                );

        if (foundProfile != null)
        {
            this.SetProfileInfo(foundProfile);
        }
        else
        {
            Debug.Log("redirecting to select profile scene");
            SceneHandler.GetInstance().LoadChangeProfileScene();
        }
    }

    private void SetProfileInfo(Player player)
    {
        Sprite image = null;

        try
        {
            var texture = Resources.Load<Texture2D>(player.ImageName);
            if (texture != null)
            {
                image = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(texture.width / 2, texture.height / 2));
            }
        }
        catch
        {

        }

        this.ProfileImage.sprite = image;
        this.ProfileImage.color = Color.white;
        this.ProfileNameText.text = player.Name;

        ProfileInfoSelectionProvider.MainProfile = new ProfileInfo
        {
            ID = player.Id,
            Image = image,
            Name = player.Name
        };
    }

    public void Application_Exit()
    {
        SceneHandler.GetInstance().ApplicationQuit();
    }

    public void LoadSinglePlayerScene()
    {
        ProfileInfoSelectionProvider.SetMainAsP1();

        SceneHandler.GetInstance().StartP1Game();
    }

    public void LoadSettings()
    {
        SceneHandler.GetInstance().LoadSettings();
    }

    public void LoadMultiplayerScene()
    {
        SceneHandler.GetInstance().LoadP2ProfileSelection();
    }

    public void LoadAboutScene()
    {
        SceneHandler.GetInstance().LoadAbout();
    }

    public void LoadScoreboard()
    {
        SceneHandler.GetInstance().LoadScoreboard();
    }

    public void MusicToggle_OnValueChange()
    {
        SoundManager.GetInstance().ToggleMusic();
        this.MusicToggleButton.AdjustToggleLabel();
    }

    public void LoadNewProfileScene()
    {
        SceneHandler.GetInstance().LoadNewProfileScene();
    }

    public void LoadChangeProfileScene()
    {
        SceneHandler.GetInstance().LoadChangeProfileScene();
    }
}
