﻿using Assets.SCRIPTS.Interfaces;
using Assets.SCRIPTS.Models;
using Assets.SCRIPTS.Repositories;
using Assets.SCRIPTS.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.ViewControllers
{
    public class SettingsSceneViewController : MonoBehaviour
    {
        public Toggle MusicToggle;
        public Slider MusicVolumeSlider;
        public GameObject KeybindInfoPanel;
        public KeybindTextModel[] KeybindTexts;

        private bool isListening = false;
        private Controls controlToLookFor { get; set; }

        private IKeybindRepository keybindRepository;
        private SoundManager soundManager;

        private void Awake()
        {
            this.soundManager = SoundManager.GetInstance();
            this.keybindRepository = new PlayerPrefKeybindRepository();

            foreach (var keybindText in this.KeybindTexts)
            {
                keybindText.UIText.text = this.keybindRepository.GetName(keybindText.Keybind);
            }

            if (this.soundManager != null)
            {
                StartCoroutine(this.MusicToggle.SetDefaultToggleState_CoRoutine(!this.soundManager.IsBackgroundMusicMuted()));
                StartCoroutine(this.MusicVolumeSlider.SetInitialValue_CoRoutine(this.GetSliderValueForVolume()));
            }
        }

        private void Update()
        {
            if (this.controlToLookFor != Controls.None && isListening && Input.anyKeyDown)
            {
                foreach (KeyCode key in Enum.GetValues(typeof(KeyCode)))
                {
                    var keyId = (int)key;
                    if ((keyId < 330 || keyId > 349) && Input.GetKeyDown(key))
                    {
                        this.isListening = false;

                        foreach (Controls keybind in Enum.GetValues(typeof(Controls)))
                        {
                            KeybindTextModel keybindInfo = this.KeybindTexts.FirstOrDefault(it => it.Keybind == this.controlToLookFor);

                            if (keybindInfo != null)
                            {
                                keybindInfo.UIText.text = key.ToString();
                                this.keybindRepository.SetKey(keybindInfo.Keybind, key);

                                this.controlToLookFor = Controls.None;
                                this.isListening = false;
                                this.KeybindInfoPanel.SetActive(false);

                                return;
                            }
                        }

                        this.KeybindInfoPanel.SetActive(false);
                    }
                }
            }
        }

        public void SetKeyToListenFor(string controlName)
        {
            var found = Controls.None;
            try
            {
                found = (Controls)Enum.Parse(typeof(Controls), controlName, true);
            }
            catch
            {
                found = Controls.None;
            }

            if (found != Controls.None && this.isListening == false)
            {
                this.KeybindInfoPanel.SetActive(true);
                this.controlToLookFor = found;
                StartCoroutine(WaitUntilKeyIsDown());
            }
            else
            {
                this.isListening = false;
            }
        }

        public void MusicToggle_OnValueChange()
        {
            if (this.soundManager != null)
            {
                this.soundManager.ToggleMusic();
                this.MusicToggle.AdjustToggleLabel();
            }
        }

        public void MusicVolume_OnValueChange()
        {
            if (this.soundManager != null)
            {
                this.soundManager.SetVolume(this.MusicVolumeSlider.value / this.MusicVolumeSlider.maxValue);
            }
        }

        public void BackButton_OnClick()
        {
            this.keybindRepository.SaveChanges();
            SceneHandler.GetInstance().LoadMainMenu();
        }

        private IEnumerator<bool> WaitUntilKeyIsDown()
        {
            while (Input.anyKeyDown)
            {
                this.isListening = false;
                yield return false;
            }

            this.isListening = true;
            yield return true;
        }

        private float GetSliderValueForVolume()
        {
            if(this.soundManager != null)
            {
               return this.soundManager.GetVolume() * this.MusicVolumeSlider.maxValue;
            }

            return 0;
        }
    }
}
