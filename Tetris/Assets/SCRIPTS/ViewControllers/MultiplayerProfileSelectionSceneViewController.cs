﻿using Assets.Scripts.ViewControllers;
using Assets.SCRIPTS.Repositories;
using Assets.SCRIPTS.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MultiplayerProfileSelectionSceneViewController : MonoBehaviour
{
    public GameObject PrefabItem;
    public GameObject ContentContainer;
    public Text P1NameText;
    public Text P2NameText;
    public Image P1Image;
    public Image P2Image;

    public Button StartButton;

    private ProfileRepository profileRepository;
    private Dictionary<string, Sprite> profileImages;
    private IControllerScheme P1ControllerScheme;
    private IControllerScheme P2ControllerScheme;

    // Use this for initialization
    void Start()
    {
        this.profileRepository = new ProfileRepository();
        this.profileImages = new Dictionary<string, Sprite>();
        this.P1ControllerScheme = new P1ControllerScheme();
        this.P2ControllerScheme = new P2ControllerScheme();

        this.LoadProfiles();

        if (ProfileInfoSelectionProvider.P1ID > 0 && ProfileInfoSelectionProvider.P2ID > 0)
        {
            if (this.StartButton != null)
            {
                this.StartButton.interactable = true;
            }
        }
    }

    private void LoadProfiles()
    {
        var profiles = this.profileRepository.GetPlayers();

        for (var index = 0; index < profiles.Count; index++)
        {
            var info = profiles.ElementAt(index);
            var option = Instantiate(this.PrefabItem, this.ContentContainer.transform);
            var trigger = option.AddComponent<EventTrigger>();
            trigger.triggers = this.GetComponent<EventTrigger>().triggers;

            if (index == 0)
            {
                EventSystem.current.firstSelectedGameObject = option.gameObject;
            }

            var textObjs = option.GetComponentsInChildren<Text>(true);
            var idObj = textObjs.FirstOrDefault(it => it.name == "ID");
            var nameObj = textObjs.FirstOrDefault(it => it.name == "Name");

            var background = option.transform.GetComponentsInChildren<Image>().FirstOrDefault(it => it.name == "Background");

            if (idObj == null || nameObj == null || background == null)
            {
                Debug.Log("in sufficient UI obj found");
                continue;
            }

            idObj.text = info.Id.ToString();
            nameObj.text = info.Name;


            Sprite image;

            if (!this.profileImages.TryGetValue(info.ImageName, out image))
            {
                var t = Resources.Load<Texture2D>(info.ImageName);
                if (t != null)
                {
                    var img = Sprite.Create(t, new Rect(0, 0, t.width, t.height), new Vector2(t.width / 2, t.height / 2));
                    this.profileImages.Add(info.ImageName, img);
                    image = img;
                }
            }

            if (image != null)
            {
                background.sprite = image;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (this.P1ControllerScheme.IsProfileSelectionButtonUp())
        {
            this.SetPlayer(EventSystem.current.currentSelectedGameObject, PlayerScheme.P1);
        }

        if (this.P2ControllerScheme.IsProfileSelectionButtonUp())
        {
            this.SetPlayer(EventSystem.current.currentSelectedGameObject, PlayerScheme.P2);
        }

    }

    public void SetPlayer(GameObject buttonObj, PlayerScheme player)
    {
        Button button = null;
        if (buttonObj != null)
        {
            button = buttonObj.GetComponent<Button>();
        } 
        if (button != null && button.gameObject.name.StartsWith("SelectProfileButton"))
        {
            var name = button.transform.GetComponentsInChildren<Text>().FirstOrDefault(it => it.name == "Name");
            var image = button.transform.GetComponentsInChildren<Image>().FirstOrDefault(it => it.name == "Background");
            var idText = button.transform.GetComponentsInChildren<Text>(true).FirstOrDefault(it => it.name == "ID");

            if (idText != null)
            {
                switch (player)
                {
                    case PlayerScheme.P1:
                        {
                            ProfileInfoSelectionProvider.P1ID = Convert.ToInt32(idText.text);
                            ProfileInfoSelectionProvider.P1Image = image.sprite;
                            this.P1Image.sprite = image.sprite;
                            this.P1NameText.text = name.text;
                        }
                        break;
                    case PlayerScheme.P2:
                        {
                            ProfileInfoSelectionProvider.P2ID = Convert.ToInt32(idText.text);
                            ProfileInfoSelectionProvider.P2Image = image.sprite;
                            this.P2Image.sprite = image.sprite;
                            this.P2NameText.text = name.text;
                        }
                        break;
                    default:
                        return;
                }

                if (ProfileInfoSelectionProvider.P1ID > 0 && ProfileInfoSelectionProvider.P2ID > 0)
                {
                    if (this.StartButton != null)
                    {
                        this.StartButton.interactable = true;
                    }
                }
            }
        }
    }

    public void BackButton_OnClick()
    {
        SceneHandler.GetInstance().LoadMainMenu();
    }

    public void StartButton_OnClick()
    {
        if (ProfileInfoSelectionProvider.P1ID > 0 && ProfileInfoSelectionProvider.P2ID > 0)
        {
            SceneHandler.GetInstance().StartP2Game();
        }
    }
}

