﻿using Assets.Scripts.ViewControllers;
using Assets.SCRIPTS.Models;
using Assets.SCRIPTS.Repositories;
using Assets.SCRIPTS.Utility;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class NewProfileSceneViewController : MonoBehaviour {

    public GameObject ContentContainer;
    public ToggleGroup ToggleGroup;
    public GameObject ImageTogglePrefab;

    public Button CreateButton;
    public InputField NameInput;
    public Sprite[] sprites;

    private ProfileRepository profileRepository;

	// Use this for initialization
	void Start () {
        this.profileRepository = new ProfileRepository();
        this.LoadImages();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadImages()
    {
        for(var index = 0; index < this.sprites.Length; index++)
        {
            var img = this.sprites[index];
            var imgToggleObj = Instantiate(this.ImageTogglePrefab, this.ContentContainer.transform);
            var imgToggle = imgToggleObj.GetComponent<Toggle>();
            imgToggle.group = this.ToggleGroup;

            if(index == 0)
            {
                this.NameInput.GetComponent<Selectable>().navigation = new Navigation() { mode = Navigation.Mode.Explicit, selectOnDown = imgToggle.GetComponent<Selectable>() };
            }
            else if(index == this.sprites.Length - 1)
            {
                this.CreateButton.GetComponent<Selectable>().navigation = new Navigation() { mode = Navigation.Mode.Explicit, selectOnUp = imgToggle.GetComponent<Selectable>() };
            }

            var imgToggleSprite = imgToggle.GetComponentsInChildren<Image>().FirstOrDefault(it => it.name == "Background");

            if(imgToggleSprite != null)
            {
                imgToggleSprite.sprite = img;
            }
        }
    }


    public void CreateButton_OnClick()
    {
        var activeToggle = this.ToggleGroup.ActiveToggles().FirstOrDefault();

        if(activeToggle == null)
        {
            return;
        }

        var name = this.NameInput.text;

        if (string.IsNullOrEmpty(name))
        {
            return;
        }

        var imageSprite = activeToggle.GetComponentsInChildren<Image>().FirstOrDefault(it => it.name == "Background").sprite;
        var imageName = imageSprite.name;

        if (string.IsNullOrEmpty(imageName))
        {
            return;
        }

        var player = this.profileRepository.CreateProfile(name, imageName);

        if(player == null)
        {
            return;
        }

        this.profileRepository.SetProfileAsMain(player.Id);
        ProfileInfoSelectionProvider.MainProfile = new ProfileInfo { ID = player.Id, Image = imageSprite };


        if (!string.IsNullOrEmpty(SceneHandler.GetInstance().PreviousSceneName))
        {
            SceneHandler.GetInstance().LoadPreviousScene();
        }
        else
        {
            SceneHandler.GetInstance().LoadMainMenu();
        }
    }
}
