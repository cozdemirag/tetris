﻿using System.Linq;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Components
{
    [RequireComponent(typeof(ScrollRect))]
    public class ScrollViewContentFollower : MonoBehaviour
    {
        [SerializeField]
        private float m_lerpTime;

        private ScrollRect ScrollRect;
        private GameObject[] ContentItems;
        private int index = 0;
        private int maxIndex;
        private float verticalPosition = 1f;
        private float horizontalPosition = 0f;

        public void Awake()
        {
        }
        public void Start()
        {
            this.ScrollRect = GetComponent<ScrollRect>();
            this.ContentItems = this.GetContentItems();

            if(this.ContentItems.Length > 0)
            {
                this.maxIndex = this.ContentItems.Length - 1;
            }
        }

        public void LateInit()
        {
            this.ContentItems = this.GetContentItems();

            if (this.ContentItems.Length > 0)
            {
                this.maxIndex = this.ContentItems.Length - 1;
            }
        }

        private GameObject[] GetContentItems()
        {
            var items = new GameObject[this.ScrollRect.content.childCount];

            for(var i = 0; i < this.ScrollRect.content.childCount; i++)
            {
                items[i] = this.ScrollRect.content.GetChild(i).gameObject;
            }

            return items;
        }

        public void Update()
        {
            this.LerpScrollRect();
        }

        private void CalculateScrollPosition()
        {
            this.verticalPosition = 1f - ((float)this.index / this.maxIndex);
            this.horizontalPosition = (float)this.index / this.maxIndex;
        }

        private void LerpScrollRect()
        {
            if(this.horizontalPosition != this.ScrollRect.horizontalNormalizedPosition)
            {
                this.ScrollRect.horizontalNormalizedPosition = Mathf.Lerp(this.ScrollRect.horizontalNormalizedPosition, this.horizontalPosition, Time.deltaTime / m_lerpTime);
            }

            if (this.verticalPosition != this.ScrollRect.verticalNormalizedPosition)
            {
                this.ScrollRect.verticalNormalizedPosition = Mathf.Lerp(this.ScrollRect.verticalNormalizedPosition, this.verticalPosition, Time.deltaTime / m_lerpTime);
            }
        }

        private int GetButtonIndex(int direction)
        {
            return this.index = Mathf.Clamp(this.index + direction, 0, this.maxIndex);
        }

        // TODO add Dictionay<int, GameObject> to feed the items from the editor.
        private int FindIndexByGameObject(GameObject button)
        {
            for(var i = 0; i < this.ContentItems.Length; i++)
            {
                var item = this.ContentItems[i];

                if(button == item)
                {
                    return i;
                }
                else
                {
                    if(item.GetComponentsInChildren<Button>().FirstOrDefault(it => it.gameObject == button) != null)
                    {
                        return i;
                    }
                }
            }

            return 0;
        }

        public void LerpToObject(BaseEventData eventData)
        {
            this.LerpToButtonAt(eventData.selectedObject);
        }

        private void LerpToButtonAt(GameObject selectedObject)
        {
            if(this.ContentItems.Length > 0)
            {
                var index = FindIndexByGameObject(selectedObject);
                this.index = Mathf.Clamp(index, 0, this.maxIndex);

                if (!this.ScrollRect.viewport.rect.Contains(selectedObject.transform.position))
                {
                    this.horizontalPosition = this.index / (this.maxIndex * 1f);
                    this.verticalPosition = 1f - (float)Math.Round((this.index / (this.maxIndex * 1d)), 1);
                }
            }
        }
    }
}
