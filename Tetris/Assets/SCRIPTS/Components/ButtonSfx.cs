﻿using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonSfx : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, ISelectHandler, ISubmitHandler
{
    public AudioClip OnSelectClip;
    public AudioClip OnClickClip;

    private Dictionary<ButtonSfxEvent, AudioClip> sfx = new Dictionary<ButtonSfxEvent, AudioClip>();
    private Button Button { get { return this.GetComponent<Button>(); } }
    private AudioSource source;

    // Use this for initialization
    void Start()
    {
        this.sfx = new Dictionary<ButtonSfxEvent, AudioClip>();

        if (OnSelectClip != null)
        {
            this.sfx.Add(ButtonSfxEvent.OnSelected, this.OnSelectClip);
        }

        if (OnClickClip != null)
        {
            this.sfx.Add(ButtonSfxEvent.OnClick, this.OnClickClip);
        }
        this.source = this.Button.gameObject.AddComponent<AudioSource>();
        this.source.volume = 0.5f;
        this.source.playOnAwake = false;
        this.source.enabled = false;
    }

    public void OnPointerEnter(PointerEventData ped)
    {
        this.PlayOnSelectSfx();
    }

    public void OnPointerDown(PointerEventData ped)
    {
        this.PlayOnClickSfx();
    }

    public void OnSelect(BaseEventData eventData)
    {
        this.PlayOnSelectSfx();
    }

    public void OnSubmit(BaseEventData eventData)
    {
        this.PlayOnClickSfx();
    }

    private void PlayOnSelectSfx()
    {
        this.Button.StartCoroutine(Play(ButtonSfxEvent.OnSelected));
    }

    private void PlayOnClickSfx()
    {
        if (this.Button.IsActive())
        {
            try
            {
                this.Button.StartCoroutine(Play(ButtonSfxEvent.OnClick));
            }
            catch
            {

            }
        }
    }

    private IEnumerator Play(ButtonSfxEvent sfxEvent)
    {
        if (this.sfx.ContainsKey(sfxEvent))
        {
            this.source.enabled = true;

            this.source.clip = this.sfx[sfxEvent];
            this.source.Play();

            yield return null;

            while (this.source.isPlaying)
            {
                yield return null;
            }
        }
    }
}
