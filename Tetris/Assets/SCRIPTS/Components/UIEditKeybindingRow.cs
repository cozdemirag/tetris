﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Components
{
    [RequireComponent(typeof(RectTransform))]
    public class UIEditKeybindingRow : MonoBehaviour
    {
        public RectTransform.Axis RatioAxis = RectTransform.Axis.Horizontal;

        public float Width;
        [Range(0.1f, 0.9f)]
        public float TextSizeRatio = 0.75f;

        public string Label;
        public string ButtonLabel;

        private Button Button { get { return this.GetComponent<Button>(); } }
        private Text ButtonText { get { return this.Button.GetComponent<Text>(); } }

        public void Awake()
        {
            var textWidth = Width * this.TextSizeRatio;
            var buttonWidth = Width * (1f - this.TextSizeRatio);

            var labelText = this.gameObject.AddComponent<Text>();
            labelText.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RatioAxis, textWidth);
            labelText.text = this.Label;

            var button = this.gameObject.AddComponent<Button>();
            button.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RatioAxis, buttonWidth);
            button.GetComponent<Text>().text = this.ButtonLabel;
        }

        public void UpdateButtonText(string val)
        {
            this.ButtonText.text = val;
        }
    }
}
