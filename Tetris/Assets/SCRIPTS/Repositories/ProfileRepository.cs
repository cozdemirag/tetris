﻿using Assets.SCRIPTS.Models;
using Assets.SCRIPTS.Utility;
using Mono.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.SCRIPTS.Repositories
{
    public class ProfileRepository
    {
        private string connectionString = "URI=file:" + Application.dataPath + "/scoreboard.tdb";

        private QueryFormatter queryFormatter;

        public ProfileRepository()
        {
            this.queryFormatter = new QueryFormatter();
            this.InitTable(this.queryFormatter.GetQuery(this.InitPlayerProcName));
        }

        private void InitTable(string query)
        {
            try
            {
                using (var connection = new SqliteConnection(connectionString))
                {
                    connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }

        public void SetProfileAsMain(int id)
        {
            try
            {
                using (var connection = new SqliteConnection(connectionString))
                {
                    using (var command = connection.CreateCommand())
                    {
                        connection.Open();
                        command.CommandText = this.queryFormatter.FormatQuery(SetProfuleAsMainProcName, id);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }

        public Player CreateProfile(string name, string imageName, bool isMain = true)
        {
            try
            {
                using (var connection = new SqliteConnection(connectionString))
                {
                    using (var command = connection.CreateCommand())
                    {
                        connection.Open();

                        command.CommandText = this.queryFormatter.FormatQuery(AddProfileProcName, name, imageName, isMain ? 1 : 0);
                        var reader = command.ExecuteReader();

                        var player = this.MapTo(reader).FirstOrDefault();
                        return player;
                    }
                }
            }
            catch(Exception ex)
            {

            }

            return null;
        }

        public List<Player> GetPlayers()
        {
            try
            {
                using (var connection = new SqliteConnection(connectionString))
                {
                    using (var command = connection.CreateCommand())
                    {
                        connection.Open();

                        command.CommandText = this.queryFormatter.GetQuery(GetPlayersProcName);
                        var reader = command.ExecuteReader();

                        return MapTo(reader);
                    }
                }
            }
            catch
            {

            }

            return new List<Player>();
        }

        private List<Player> MapTo(SqliteDataReader reader)
        {
            var result = new List<Player>();

            while (reader.Read())
            {
                result.Add(new Player { Id = reader.GetInt32(0), Name = reader.GetString(1), ImageName = reader.GetString(2), IsMain = reader.GetBoolean(3) });
            }

            return result;
        }

        private string InitPlayerProcName = "createPlayer";
        private string GetPlayersProcName = "getPlayers";
        private string AddProfileProcName = "insertProfile";
        private string SetProfuleAsMainProcName = "setProfileAsMain";
    }
}
