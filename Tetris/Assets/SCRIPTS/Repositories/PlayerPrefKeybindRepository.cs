﻿using Assets.SCRIPTS.Interfaces;
using Assets.SCRIPTS.Utility;
using UnityEngine;

namespace Assets.SCRIPTS.Repositories
{
    public class PlayerPrefKeybindRepository : IKeybindRepository
    {
        public string GetName(Controls keybind)
        {
            var val = PlayerPrefs.GetInt(keybind.ToString());

            if (val == 0)
            {
                return "N/A";
            }

            return ((KeyCode)val).ToString();
        }

        public void SetKey(Controls keybind, KeyCode keycode)
        {
            PlayerPrefs.SetInt(keybind.ToString(), (int)keycode);
        }

        public void SaveChanges()
        {
            PlayerPrefs.Save();
        }
    }
}
