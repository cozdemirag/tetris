﻿using Assets.SCRIPTS.Models;
using Assets.SCRIPTS.Utility;
using Mono.Data.Sqlite;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ScoreboardRepository  {

    private string connectionString = "URI=file:" + Application.dataPath + "/scoreboard.tdb";

    private QueryFormatter queryFormatter;

    public ScoreboardRepository()
    {
        this.queryFormatter = new QueryFormatter();

        this.InitTable(this.queryFormatter.GetQuery(this.InitLeagueProcName));
        this.InitTable(this.queryFormatter.GetQuery(this.InitPlayerProcName));
        this.InitTable(this.queryFormatter.GetQuery(this.InitLeaguePlayerProcName));
        this.InitTable(this.queryFormatter.GetQuery(this.InitVersusGameProcName));
        this.InitTable(this.queryFormatter.GetQuery(this.InitVersusScoreProcName));
        this.InitTable(this.queryFormatter.GetQuery(this.InitScoreProcName));

    }

    private void InitTable(string query)
    {
        try
        {
            using (var connection = new SqliteConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    connection.Open();

                    command.CommandText = query;
                    command.ExecuteNonQuery();
                }
            }
        }
        catch (Exception ex)
        {
            var message = ex.Message;
        }
    }

    public List<P1ScoreboardItem> GetTop10P1Scores()
    {
        try
        {
            using (var connection = new SqliteConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    connection.Open();

                    command.CommandText = this.queryFormatter.GetQuery(this.GetTop10P1ScoreProcName);
                    var reader = command.ExecuteReader();

                    return this.MapTo(reader);
                }
            }
        }
        catch
        {

        }

        return new List<P1ScoreboardItem>();
    }

    public bool AddScore(int score, int playerId)
    {
        try
        {
            using (var connection = new SqliteConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    connection.Open();

                    command.CommandText = this.queryFormatter.FormatQuery(this.AddScoreProcName, score, playerId);
                    return command.ExecuteNonQuery() == 1;
                }
            }
        }
        catch
        {

        }

        return false;
    }

    private string InitLeagueProcName = "createLeague";
    private string InitLeaguePlayerProcName = "createLeaguePlayer";
    private string InitPlayerProcName = "createPlayer";
    private string InitScoreProcName = "createScore";
    private string InitVersusGameProcName = "createVersusGame";
    private string InitVersusScoreProcName = "createVersusScore";
    private string AddScoreProcName = "AddScore";
    private string GetTop10P1ScoreProcName = "getTop10Scores";


    private List<P1ScoreboardItem> MapTo(SqliteDataReader reader)
    {
        var result = new List<P1ScoreboardItem>();

        var index = 1;
        while (reader.Read())
        {
            result.Add(new P1ScoreboardItem { SequenceOnBoard = index, Score = reader.GetInt32(0), PlayerName = reader.GetString(1) });
            index++;
        }

        return result;
    }
}


