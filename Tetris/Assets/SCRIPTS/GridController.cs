﻿using Assets.SCRIPTS.Interfaces;
using Assets.SCRIPTS.Models;
using Assets.SCRIPTS.Utility;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GridController : MonoBehaviour
{
    private Board board;

    private ITetriminoGenerator tetriminoGenerator;

    public GameObject I_minoPrefab;
    public GameObject J_minoPrefab;
    public GameObject L_minoPrefab;
    public GameObject Z_minoPrefab;
    public GameObject S_minoPrefab;
    public GameObject O_minoPrefab;
    public GameObject T_minoPrefab;

    public PlayerScheme PlayerScheme;
    public Image PlayerImage;
    public Text ScoreText;
    public Text LinesText;
    public Text LevelText;
    public Text DraughtText;
    public GameObject NextTetriminoSpawn;
    public GameObject HeldTetriminoSpawn;
    public GameObject SpawnPoint;

    public EventHandler<PlayerGridArgs> OnGameOver_GridEvent;
    public EventHandler<PlayerGridArgs> TogglePause_GridEvent;

    private int level;
    private int lines;
    private int score;
    private bool isGameOver = false;
    private bool isGamePaused = false;

    private TetriminoController nextTetrimino;
    private IProfileInfoSelection profileInfo;
    private bool canGenerateNextTetrimino = false;
    private int LevelUpLimit;
    private int rowClearedCount = 0;
    private float tetriminoFallPeriod = 1.0f;
    private Dictionary<Tetriminos, GameObject> minoPrefabs;


    private void Awake()
    {
        this.minoPrefabs = new Dictionary<Tetriminos, GameObject>();
        this.minoPrefabs.Add(Tetriminos.I, I_minoPrefab);
        this.minoPrefabs.Add(Tetriminos.J, J_minoPrefab);
        this.minoPrefabs.Add(Tetriminos.L, L_minoPrefab);
        this.minoPrefabs.Add(Tetriminos.O, O_minoPrefab);
        this.minoPrefabs.Add(Tetriminos.S, S_minoPrefab);
        this.minoPrefabs.Add(Tetriminos.T, T_minoPrefab);
        this.minoPrefabs.Add(Tetriminos.Z, Z_minoPrefab);
        this.profileInfo = new ProfileInfoSelectionProvider().GetProfileInfoSelection(this.PlayerScheme);
        this.tetriminoGenerator = new TetriminoGenerator(this.minoPrefabs.Count);
        this.board = new Board(10, 20, this.transform.position.x, this.transform.position.y, this.Board_OnGridClearEventHandler, this.Board_OnGameOverEventHandler, this.SpawnPoint.transform.position);
        this.Initialize();
    }

    // Use this for initialization
    void Start()
    {
        GenerateTetriminos();
    }

    // Update is called once per frame
    void Update()
    {
        if (!this.isGameOver)
        {
            if (this.GetControllerScheme().IsPauseButtonUp())
            {
                if (this.TogglePause_GridEvent != null)
                {
                    this.TogglePause_GridEvent(this,
                        new PlayerGridArgs
                        {
                            Player = this.PlayerScheme,
                            Id = this.profileInfo.GetId(),
                            Image = this.profileInfo.GetImage(),
                            Level = this.level,
                            Score = this.score,
                            LinesCleared = this.lines
                        });
                }
            }

            if (canGenerateNextTetrimino)
            {
                this.GenerateTetriminos();
                this.canGenerateNextTetrimino = false;
            }
        }
    }

    private void GenerateTetriminos()
    {
        this.UpdateDraughtText();

        if (this.nextTetrimino == null)
        {
            var current = this.SpawnMino();
            current.transform.parent = this.transform;
            current.MakeCurrent();
            var next = this.SpawnMino();
            this.nextTetrimino = next;
        }
        else
        {
            this.nextTetrimino.MakeCurrent();
            this.nextTetrimino.transform.parent = this.transform;
            this.nextTetrimino = this.SpawnMino();
        }
    }

    private void TetriminoController_OnMinoBecomingInActiveEventHandler(object sender, TetriminoBecomeInActiveArgs args)
    {
        SoundManager.GetInstance().PlayTetriminoDrop();

        var tetrimino = (TetriminoController)sender;
        this.board.PlaceMinos(tetrimino.transform);
        UpdateScore(args);
        if (!this.isGameOver)
        {
            this.canGenerateNextTetrimino = true;
        }
    }

    private TetriminoController SpawnMino()
    {
        var index = this.tetriminoGenerator.GetTetriminoIndex();

        return this.SpawnSpecificTetrimino(index);
    }

    private TetriminoController SpawnSpecificTetrimino(Tetriminos index)
    {
        var mino = GameObject.Instantiate(this.minoPrefabs[index], this.NextTetriminoSpawn.transform.position, Quaternion.identity, this.transform).GetComponent<TetriminoController>();
        mino.OnBecomingInActive += TetriminoController_OnMinoBecomingInActiveEventHandler;
        mino.Init(this.board, this.GetControllerScheme(), this.level, this.tetriminoFallPeriod);
        return mino;
    }

    private IControllerScheme GetControllerScheme()
    {
        switch (this.PlayerScheme)
        {
            case PlayerScheme.P2:
                return new P2ControllerScheme();
            default:
                return new P1ControllerScheme();
        }
    }

    private void Board_OnGridClearEventHandler(object sender, RowClearedArgs args)
    {
        var rows = args.RowsCleared;
        this.rowClearedCount += rows.Count;
        this.lines += rows.Count;
        this.score += this.CalculateScore(args);
        this.ScoreText.text = this.score.ToString();
        this.LinesText.text = this.lines.ToString();


        SoundManager.GetInstance().PlayRowClearSfx(rows.Count);

        if (this.rowClearedCount >= LevelUpLimit)
        {
            this.rowClearedCount = 0;
            this.UpdateLevel();
            this.tetriminoFallPeriod *= 0.80f;
        }
    }

    private void Board_OnGameOverEventHandler(object sender, EventArgs args)
    {
        if (!this.isGameOver)
        {
            SoundManager.GetInstance().PlayGameOver();

            this.isGameOver = true;
            // enable ui 
            // show scores
            // show time took
            // show how many rows cleared.
            // ask for name
            // save and quit to main menu


            if (this.OnGameOver_GridEvent != null)
            {
                this.OnGameOver_GridEvent(this,
                    new PlayerGridArgs
                    {
                        Player = this.PlayerScheme,
                        Id = this.profileInfo.GetId(),
                        Image = this.profileInfo.GetImage(),
                        Level = this.level,
                        Score = this.score,
                        LinesCleared = this.lines
                    });
            }
        }
    }

    private void UpdateScore(RowClearedArgs args)
    {
        this.score += this.CalculateScore(args);
        this.ScoreText.text = this.score.ToString();
    }

    private void UpdateScore(TetriminoBecomeInActiveArgs args)
    {
        if(args.LevelsHardDropped > 0)
        {
            this.score += Mathf.Abs(2 * args.LevelsHardDropped);
        }
        else
        {
            this.score += Mathf.Abs(args.LevelsSoftDropped);
        }

        this.ScoreText.text = this.score.ToString();
    }

    private int CalculateScore(RowClearedArgs args)
    {
        switch (args.RowsCleared.Count)
        {
            case 1:
                return Convert.ToInt32(100 * this.level * args.BackToBackClearBonus);
            case 2:
                return Convert.ToInt32(300 * this.level * args.BackToBackClearBonus);
            case 3:
                return Convert.ToInt32(500 * this.level * args.BackToBackClearBonus);
            case 4:
                return Convert.ToInt32(800 * this.level * args.BackToBackClearBonus);
            default:
                return 0;
        }
    }

    private void Initialize()
    {
        this.level = 1;
        this.lines = 0;
        this.score = 0;
        this.LevelUpLimit = this.level * 5;
        InitializeHUD();
    }

    private void UpdateLevel()
    {
        this.level++;
        this.LevelUpLimit = this.level * 5;
        this.LevelText.text = this.level.ToString();
    }

    private void InitializeHUD()
    {
        this.DraughtText.text = string.Empty;
        this.ScoreText.text = this.score.ToString();
        this.LinesText.text = this.lines.ToString();
        this.LevelText.text = this.level.ToString();
        this.PlayerImage.sprite = this.profileInfo.GetImage();
    }

    private void UpdateDraughtText()
    {
        var count = this.tetriminoGenerator.GetIDraughtCountdown();

        if(count == null)
        {
            this.DraughtText.text = string.Empty;
        }
        else
        {
            this.DraughtText.text = count.ToString();
        }

    }

    public void TogglePause()
    {
        this.isGamePaused = !this.isGamePaused;

        foreach (var child in this.transform.GetComponentsInChildren<TetriminoController>(false))
        {
            child.ToggleBeingPaused();
        }
    }
}