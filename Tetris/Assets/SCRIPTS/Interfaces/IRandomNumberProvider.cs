﻿namespace Assets.SCRIPTS.Interfaces
{
    public interface IRandomNumberProvider
    {
        int GetNext(int lowBound, int highBound);
    }
}
