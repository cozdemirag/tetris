﻿using Assets.SCRIPTS.Utility;
using UnityEngine;

namespace Assets.SCRIPTS.Interfaces
{
    public interface IKeybindRepository
    {
        string GetName(Controls keybind);

        void SetKey(Controls keybind, KeyCode keycode);

        void SaveChanges();
    }
}
