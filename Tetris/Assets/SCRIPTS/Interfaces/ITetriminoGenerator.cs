﻿using Assets.SCRIPTS.Utility;

namespace Assets.SCRIPTS.Interfaces
{
    public interface ITetriminoGenerator
    {
        Tetriminos GetTetriminoIndex();

        int? GetIDraughtCountdown();
    }
}
