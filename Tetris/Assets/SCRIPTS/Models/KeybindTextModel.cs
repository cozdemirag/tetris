﻿using Assets.SCRIPTS.Utility;
using System;
using UnityEngine.UI;

namespace Assets.SCRIPTS.Models
{
    [Serializable]
    public class KeybindTextModel
    {
        public Controls Keybind;
        public Text UIText;
    }
}
