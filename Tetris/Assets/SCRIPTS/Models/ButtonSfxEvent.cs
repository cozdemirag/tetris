﻿namespace Assets.Scripts.Models
{
    public enum ButtonSfxEvent
    {
        OnSelected,
        OnClick
    }
}
