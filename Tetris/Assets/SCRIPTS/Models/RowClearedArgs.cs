﻿using System;
using System.Collections.Generic;

namespace Assets.SCRIPTS.Models
{
    public class RowClearedArgs : EventArgs
    {
        public float BackToBackClearBonus { get; set; }

        public List<int> RowsCleared { get; set; }
    }
}
