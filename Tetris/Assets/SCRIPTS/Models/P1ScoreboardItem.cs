﻿namespace Assets.SCRIPTS.Models
{
    public class P1ScoreboardItem
    {
        public int SequenceOnBoard;
        public int Score;
        public string PlayerName;
    }
}
