﻿using UnityEngine;

namespace Assets.SCRIPTS.Models
{
    public class ProfileInfo
    {
        public int ID { get; set; }

        public Sprite Image { get; set; }

        public string Name { get; set; }
    }
}
