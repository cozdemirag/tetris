﻿using System;

namespace Assets.SCRIPTS.Models
{
    public class TetriminoBecomeInActiveArgs : EventArgs
    {
        public int LevelsHardDropped { get; set; }
        public int LevelsSoftDropped { get; set; }
    }
}
