﻿namespace Assets.SCRIPTS.Models
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageName { get; set; }
        public bool IsMain { get; set; }
    }
}
