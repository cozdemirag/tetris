﻿using System.Collections.Generic;
using UnityEngine.UI;

namespace Assets.SCRIPTS.Utility
{
    public static class SliderHelper
    {
        public static IEnumerator<bool> SetInitialValue_CoRoutine(this Slider slider, float val)
        {
            var sliderEvent = slider.onValueChanged;
            slider.onValueChanged = new Slider.SliderEvent();
            yield return false;

            slider.value = val;

            yield return false;

            slider.onValueChanged = sliderEvent;
        }
    }
}
