﻿using System;

namespace Assets.SCRIPTS.Utility
{
    [Serializable]
    public enum Tetriminos
    {
        Z = 0,
        T = 1,
        S = 2,
        O = 3,
        L = 4,
        J = 5,
        I = 6
    }
}
