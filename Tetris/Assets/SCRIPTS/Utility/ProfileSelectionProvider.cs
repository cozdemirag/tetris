﻿using Assets.SCRIPTS.Models;
using UnityEngine;

namespace Assets.SCRIPTS.Utility
{
    public class ProfileInfoSelectionProvider
    {
        public static ProfileInfo MainProfile = null;
        public static int P1ID = 0;
        public static int P2ID = 0;

        public static Sprite P1Image;
        public static Sprite P2Image;

        public IProfileInfoSelection GetProfileInfoSelection(PlayerScheme scheme)
        {
            if(scheme == PlayerScheme.P1)
            {
                return new P1ProfileInfoSelection();
            }

            return new P2ProfileInfoSelection();
        }

        public static void SetMainAsP1()
        {
            P1ID = MainProfile.ID;
            P1Image = MainProfile.Image;
        }

        public static void Reset()
        {
            ProfileInfoSelectionProvider.P1ID = 0;
            ProfileInfoSelectionProvider.P2ID = 0;
        }
    }

    public interface IProfileInfoSelection
    {
        int GetId();

        Sprite GetImage();
    }

    public class P1ProfileInfoSelection : IProfileInfoSelection
    {
        public int GetId()
        {
            return ProfileInfoSelectionProvider.P1ID;
        }

        public Sprite GetImage()
        {
            return ProfileInfoSelectionProvider.P1Image;
        }
    }

    public class P2ProfileInfoSelection : IProfileInfoSelection
    {
        public int GetId()
        {
            return ProfileInfoSelectionProvider.P2ID;
        }

        public Sprite GetImage()
        {
            return ProfileInfoSelectionProvider.P2Image;
        }
    }
}
