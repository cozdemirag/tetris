﻿using Assets.SCRIPTS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.SCRIPTS.Utility
{
    public class Board
    {
        private Transform[][] blocks;
        private int SizeX;
        private int SizeY;

        private int PaddingX;
        private int PaddingY;

        private Vector2 spawnPosition;

        private EventHandler<RowClearedArgs> OnRowClear;
        private EventHandler OnGameOver;
        private int placementIndex = 0;

        private int lastRowClearIndex = -1;


        public Board(int x, int y, float initX, float initY, EventHandler<RowClearedArgs> eventSub, EventHandler eventGameover, Vector2 spawnPosition)
        {
            this.SizeX = x;
            this.SizeY = y;
            this.PaddingX = (int)(-1 * (initX + 1));
            this.PaddingY = (int)(-1 * (initY + 1));

            this.spawnPosition = spawnPosition;

            this.blocks = new Transform[SizeY][];
            this.OnRowClear += eventSub;
            this.OnGameOver += eventGameover;

            for (var i = 0; i < blocks.Length; i++)
            {
                blocks[i] = new Transform[this.SizeX];
            }
        }

        private bool IsGameOver(int row, int col)
        {
            if (row >= this.SizeY || !this.IsGridEmptyAt(row, col))
            {
                return true;
            }

            return false;
        }

        public bool IsGameOver(Transform tetrimino)
        {
            foreach (Transform mino in tetrimino.transform)
            {
                var x = Mathf.RoundToInt(mino.position.x) + this.PaddingX;
                var y = Mathf.RoundToInt(mino.position.y) + this.PaddingY;

                if (this.IsGameOver(y, x))
                {
                    return true;
                }
            }

            return false;
        }

        public void PlaceMinos(Transform tetrimino)
        {
            var icount = 0;
            foreach (Transform mino in tetrimino.transform)
            {
                var x = Mathf.RoundToInt(mino.position.x) + this.PaddingX;
                var y = Mathf.RoundToInt(mino.position.y) + this.PaddingY;
                icount++;

                if (this.IsGameOver(y, x))
                {
                    if (this.OnGameOver != null)
                    {
                        this.OnGameOver(this, null);
                    }
                    return;
                }
                else
                {
                    blocks[y][x] = mino;
                }
            }

            var rowsCleared = new List<int>();

            var rowIndex = 0;
            var padding = 0;
            while (rowIndex < blocks.Length)
            {
                if (this.IsRowFullAt(rowIndex))
                {

                    this.ClearRowAt(rowIndex);
                    this.PullRowsDownFrom(rowIndex);

                    rowsCleared.Add(rowIndex + padding);

                    padding++;

                    rowIndex = 0;
                }
                else
                {
                    rowIndex++;
                }
            }

            this.placementIndex++;

            if (rowsCleared.Any() && this.OnRowClear != null)
            {
                this.OnRowClear(this, new RowClearedArgs
                {
                    RowsCleared = rowsCleared,
                    BackToBackClearBonus = lastRowClearIndex > 0 && (this.placementIndex - this.lastRowClearIndex) == 1 ? 1.5f : 1
                });
                this.lastRowClearIndex = this.placementIndex;
            }

        }

        public int GetDropPosition(Transform tetrimino)
        {
            var levelsForEachMino = new List<int>();

            foreach (Transform mino in tetrimino)
            {
                var x = Mathf.RoundToInt(mino.position.x) + this.PaddingX;
                var y = Mathf.RoundToInt(mino.position.y) + this.PaddingY;
                for (int i = 0, rowCheck = y; rowCheck >= 0; i++, rowCheck--)
                {
                    if (rowCheck > SizeY - 1)
                    {
                        continue;
                    }

                    var isNextRowEmpty = this.IsGridEmptyAt(rowCheck, x);

                    if (!isNextRowEmpty || rowCheck == 0)
                    {
                        var rowsToDrop = !isNextRowEmpty ? i - 1 : i;

                        levelsForEachMino.Add(rowsToDrop);
                        break;
                    }
                }
            }

            if (levelsForEachMino.Any())
            {
                return levelsForEachMino.Min();
            }

            return 0;
        }

        public bool HasBecomeInActive(Transform tetrimino, bool checkNext = true)
        {
            var result = false;

            foreach (Transform mino in tetrimino)
            {
                var x = Mathf.RoundToInt(mino.position.x) + this.PaddingX;
                var y = Mathf.RoundToInt(mino.position.y) + this.PaddingY;
                var rowBelow = y;

                if (checkNext)
                {
                    rowBelow -= 1;
                }

                if (y > this.SizeY || rowBelow >= this.SizeY)
                {
                    result |= false;
                    continue;
                }

                if (y == 0)
                {
                    result |= true;
                    continue;
                }

                result |= !this.IsGridEmptyAt(rowBelow, x);
            }

            return result;
        }

        // Already moved to left, not yet rendered.
        public bool ShouldRevertMoveLeft(Transform tetrimino)
        {
            var result = false;

            foreach (Transform mino in tetrimino)
            {
                var x = Mathf.RoundToInt(mino.position.x) + this.PaddingX;
                var y = Mathf.RoundToInt(mino.position.y) + this.PaddingY;

                if (x < 0)
                {
                    result |= true;
                    continue;
                }

                // Tetriminos are spawned above the grid. 
                // This is to prevent array out of bound errors. 
                // and still let the user to move left and right.
                if (y < this.SizeY)
                {
                    result |= !this.IsGridEmptyAt(y, x);
                    continue;
                }
            }

            return result;
        }

        public bool ShouldRevertRotation(Transform tetrimino)
        {
            foreach (Transform mino in tetrimino)
            {
                var x = Mathf.RoundToInt(mino.position.x) + this.PaddingX;
                var y = Mathf.RoundToInt(mino.position.y) + this.PaddingY;

                if (x < 0 || x > this.SizeX - 1 || y < 0)
                {
                    return true;
                }

                //  if(y >= this.SizeY)
                if (y < this.SizeY && !this.IsGridEmptyAt(y, x))
                {
                    return true;
                }
            }

            return false;
        }

        public bool ShouldRevertMoveRight(Transform tetrimino)
        {
            var result = false;

            foreach (Transform mino in tetrimino)
            {
                var x = Mathf.RoundToInt(mino.position.x) + this.PaddingX;
                var y = Mathf.RoundToInt(mino.position.y) + this.PaddingY;

                if (x > this.SizeX - 1)
                {
                    result |= true;
                    continue;
                }

                // Tetriminos are spawned above the grid. 
                // This is to prevent array out of bound errors. 
                // and still let the user to move left and right.
                if (y < this.SizeY)
                {
                    result |= !this.IsGridEmptyAt(y, x);
                    continue;
                }
            }

            return result;
        }

        public Vector2 GetSpawnPosition(int spawnXPadding, int spawnYPadding)
        {
            return this.spawnPosition + new Vector2(0, spawnYPadding);
            //new Vector2(this.PaddingX - spawnXPadding - 1, this.PaddingY);
        }

        public Vector2 GetNextTetriminoPosition(int spawnXPadding)
        {
            return new Vector2(-4 - spawnXPadding - 3, this.PaddingY -2 );
        }


        private bool IsGridEmptyAt(int row, int col)
        {
            return blocks[row][col] == null;
        }

        private bool IsRowFullAt(int row)
        {
            for (var y = 0; y < blocks[row].Length; y++)
            {
                if (this.IsGridEmptyAt(row, y))
                {
                    return false;
                }
            }

            return true;
        }

        private void ClearRowAt(int row)
        {
            for (var i = 0; i < this.SizeX; i++)
            {
                MonoBehaviour.DestroyObject(blocks[row][i].gameObject);
            }

            blocks[row] = new Transform[this.SizeX];
        }

        private void PullRowsDownFrom(int row)
        {
            for (var i = row; i < this.SizeY; i++)
            {
                blocks[i] = new Transform[this.SizeX];

                for (var x = 0; x < this.SizeX; x++)
                {
                    if (i >= this.SizeY - 1 || blocks[i + 1][x] == null)
                    {
                        continue;
                    }

                    blocks[i][x] = blocks[i + 1][x];
                    blocks[i][x].position += new Vector3(0, -1);
                }
            }
        }
    }
}
