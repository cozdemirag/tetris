﻿using UnityEngine;

namespace Assets.SCRIPTS.Utility
{
    public enum PlayerScheme
    {
        P1 = 1,
        P2 = 2
    }

    [SerializeField]
    public enum Controls
    {
        P1_MoveLeft = 101,
        P1_MoveRight = 102,
        P1_SoftDrop = 103,
        P1_HardDrop = 104,
        P1_Rotate = 105,
        P1_Profile_Selection = 106,
        P1_Pause = 107,
        P2_MoveLeft = 201,
        P2_MoveRight = 202,
        P2_SoftDrop = 203,
        P2_HardDrop = 204,
        P2_Rotate = 205,
        P2_Profile_Selection = 206,
        P2_Pause = 207,
        None = 0
    }

    public class P1ControllerScheme : ControllerSchemeBase
    {
        public P1ControllerScheme()
            : base(Controls.P1_Rotate.ToString(),
                  Controls.P1_MoveLeft.ToString(),
                  Controls.P1_MoveRight.ToString(),
                  Controls.P1_HardDrop.ToString(),
                  Controls.P1_HardDrop.ToString(),
                  Controls.P1_SoftDrop.ToString(),
                  Controls.P1_Profile_Selection.ToString(),
                  Controls.P1_Pause.ToString())
        {
        }
    }

    public class P2ControllerScheme : ControllerSchemeBase
    {
        public P2ControllerScheme()
            : base(Controls.P2_Rotate.ToString(),
                  Controls.P2_MoveLeft.ToString(),
                  Controls.P2_MoveRight.ToString(),
                  Controls.P2_HardDrop.ToString(),
                  Controls.P2_HardDrop.ToString(),
                  Controls.P2_SoftDrop.ToString(),
                  Controls.P2_Profile_Selection.ToString(),
                  Controls.P1_Pause.ToString())
        {
        }
    }


    public abstract class ControllerSchemeBase : IControllerScheme
    {
        private string RotateKeyName;
        private string MoveLeftKeyName;
        private string MoveRightKeyName;
        private string DropKeyName;
        private string StashKeyName;
        private string SoftDropKeyName;
        private string PauseKeyName;
        private string ProfileSelectionKeyName;

        public ControllerSchemeBase(string rotateKey, string moveLeftKey, string moveRightKey, string dropKey, string stashKey, string softDropKey, string profileSelectionKey, string pauseKey)
        {
            this.RotateKeyName = rotateKey;
            this.MoveLeftKeyName = moveLeftKey;
            this.MoveRightKeyName = moveRightKey;
            this.DropKeyName = dropKey;
            this.StashKeyName = stashKey;
            this.SoftDropKeyName = softDropKey;
            this.ProfileSelectionKeyName = profileSelectionKey;
            this.PauseKeyName = pauseKey ?? string.Empty;
        }

        public bool IsDropKeyUp()
        {
            var key = (KeyCode)PlayerPrefs.GetInt(DropKeyName);
            return Input.GetKeyDown(key);
        }

        public bool IsLeftKeyUp()
        {
            var key = (KeyCode)PlayerPrefs.GetInt(MoveLeftKeyName);

            return Input.GetKey(key);
        }

        public bool IsRightKeyUp()
        {
            var key = (KeyCode)PlayerPrefs.GetInt(MoveRightKeyName);

            return Input.GetKey(key);
        }

        public bool IsRotateKeyUp()
        {
            var key = (KeyCode)PlayerPrefs.GetInt(RotateKeyName);

            return Input.GetKeyDown(key);
        }

        public bool IsStashKeyUp()
        {
            var key = (KeyCode)PlayerPrefs.GetInt(StashKeyName);

            return Input.GetKeyDown(key);
        }

        public bool IsSoftDropKeyUp()
        {
            var key = (KeyCode)PlayerPrefs.GetInt(SoftDropKeyName);

            return Input.GetKey(key);
        }

        public bool IsPauseButtonUp()
        {
            var key = (KeyCode)PlayerPrefs.GetInt(PauseKeyName);
            return Input.GetKeyDown(key == KeyCode.None
                ? KeyCode.Escape
                : key);
        }

        public bool IsProfileSelectionButtonUp()
        {
            var key = (KeyCode)PlayerPrefs.GetInt(ProfileSelectionKeyName);

            return Input.GetKey(key);
        }
    }

    public interface IControllerScheme
    {
        bool IsLeftKeyUp();

        bool IsRightKeyUp();

        bool IsDropKeyUp();

        bool IsStashKeyUp();

        bool IsRotateKeyUp();

        bool IsSoftDropKeyUp();

        bool IsPauseButtonUp();

        bool IsProfileSelectionButtonUp();
    }
}
