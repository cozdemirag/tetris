﻿using UnityEngine;

public class CursorManager : MonoBehaviour
{
	private static CursorManager Instance;

	private CursorManager()
	{
	}

    // Use this for initialization
    void Start()
    {
		if (Instance == null) {

            Instance = this;
            //Set Cursor to not be visible
            TryDisableMouse();
			DontDestroyOnLoad (this.gameObject);
		} else {
			DestroyObject (this.gameObject);
		}
    }

    private void Update()
    {
    }


    private void TryDisableMouse()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

}
