﻿using Assets.SCRIPTS.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.SCRIPTS.Utility
{
    public class TetriminoGenerator : ITetriminoGenerator
    {
        private int sinceLastITetriminoCount;
        private Queue<int> lastindecies;
        private int ceilingNumber;
        private Array container;

        private int iterationIndex;
        private Func<Tetriminos> GetNextTetriminoDelegate;


        private readonly IRandomNumberProvider randomNumberProvider;

        private static Tetriminos[] InitialTetriminos = new Tetriminos[6]
        {
                Tetriminos.J,
                Tetriminos.T,
                Tetriminos.L,
                Tetriminos.Z,
                Tetriminos.S,
                Tetriminos.O
        };
        private static int MaxLookUpIndexCount = 6;
        private static int MaxDuplicatesAllowed = 2;
        private static int MaxITetriminoDrought = 12;
        private static int LimitedTetriminoRoundsCount = 2;

        public int? GetIDraughtCountdown()
        {
            return this.iterationIndex > LimitedTetriminoRoundsCount
                     ? MaxITetriminoDrought - this.sinceLastITetriminoCount
                     : default(int?);
        }

        public TetriminoGenerator(int max, IRandomNumberProvider randomNumberProvider)
            :this(max)
        {
            this.randomNumberProvider = randomNumberProvider;
        }

        public TetriminoGenerator(int max)
        {
            this.lastindecies = new Queue<int>(MaxLookUpIndexCount);
            this.randomNumberProvider = new UnityEngineRandomNumberProvider();
            this.container = Enum.GetValues(typeof(Tetriminos));
            this.ceilingNumber = Math.Min(Math.Abs(max), this.container.Length);
            this.sinceLastITetriminoCount = 0;
            this.iterationIndex = 0;
            this.GetNextTetriminoDelegate = this.GetTetriminosDefault;
        }

        public Tetriminos GetTetriminoIndex()
        {
            return this.GetNextTetriminoDelegate();
        }

        //private Tetriminos GetTetriminosInitial()
        //{
        //    if(this.iterationIndex >= MaxLookUpIndexCount)
        //    {
        //        this.GetNextTetriminoDelegate = this.GetTetriminosDefault;
        //    }

        //    return Return(InitialTetriminos[this.iterationIndex]);
        //}

        private Tetriminos GetTetriminosDefault()
        {
            var tetriminoSelectorValidator = this.iterationIndex > LimitedTetriminoRoundsCount
               ? (Func<Tetriminos, bool>)this.DefaultTetriminoSelectorValidator
               : this.IsInvalidTetriminoForInitialSequence;

            var tetriminoToReturn = this.GetNextTetrimino(this.container, tetriminoSelectorValidator);

            if (tetriminoToReturn == Tetriminos.I)
            {
                this.sinceLastITetriminoCount = 0;
            }
            else
            {
                this.sinceLastITetriminoCount++;
            }

            return this.Return(tetriminoToReturn);
        }

        private Tetriminos Return(Tetriminos tetriminoToReturn)
        {
            this.iterationIndex++;
            return tetriminoToReturn;
        }

        private bool IsNotRepeating(int index)
        {
            if (this.lastindecies.Count >= MaxDuplicatesAllowed)
            {
                var array = this.lastindecies.ToArray();
                return array[0] + array[1] != index * 2 && array.Count(it => it == index) < MaxDuplicatesAllowed + 1;
            }

            return true;
        }

        private bool IsInvalidTetriminoForInitialSequence(Tetriminos tetrimino)
        {
            return Tetriminos.Z != tetrimino && Tetriminos.S != tetrimino && Tetriminos.I != tetrimino;
        }

        private bool DefaultTetriminoSelectorValidator(Tetriminos tetrimino)
        {
            return true;
        }

        private Tetriminos GetNextTetrimino(Array box, Func<Tetriminos, bool> additionalFiltering = null)
        {
            if (sinceLastITetriminoCount == MaxITetriminoDrought)
            {
                return Tetriminos.I;
            }

            var index = -1;
            var tetriminoToReturn = Tetriminos.O;
            do
            {
                index = this.randomNumberProvider.GetNext(0, box.Length - 1);
                tetriminoToReturn = (Tetriminos)box.GetValue(index);
            } while (index >= 0 && index < this.ceilingNumber && !this.IsNotRepeating(index) && additionalFiltering(tetriminoToReturn));

            if (this.lastindecies.Count == MaxLookUpIndexCount)
            {
                this.lastindecies.Dequeue();
                this.lastindecies.Enqueue(index);
            }
            else
            {
                this.lastindecies.Enqueue(index);
            }

            return tetriminoToReturn;
        }
    }
}
