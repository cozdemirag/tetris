﻿using Assets.SCRIPTS.Interfaces;


namespace Assets.SCRIPTS.Utility
{
    public class UnityEngineRandomNumberProvider : IRandomNumberProvider
    {
        public int GetNext(int lowBound, int highBound)
        {
           return UnityEngine.Random.Range(lowBound, highBound);
        }
    }
}
