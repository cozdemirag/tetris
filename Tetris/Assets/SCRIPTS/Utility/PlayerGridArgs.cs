﻿using System;
using UnityEngine;

namespace Assets.SCRIPTS.Utility
{
    public class PlayerGridArgs : EventArgs
    {
        public PlayerScheme Player { get; set; }

        public int Id { get; set; }

        public Sprite Image { get; set; }

        public int Score { get; set; }

        public int LinesCleared { get; set; }

        public int Level { get; set; }
    }
}
