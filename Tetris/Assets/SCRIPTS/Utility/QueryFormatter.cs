using UnityEngine;

namespace Assets.SCRIPTS.Utility
{
    public class QueryFormatter
    {
        public string GetQuery(string filepath)
        {
            var data = Resources.Load(filepath);
            TextAsset bindata = data as TextAsset ;

            return bindata == null ? null : bindata.text;
        }

        public string FormatQuery(string filepath, params object[] parameters)
        {
            var rawQuery = this.GetQuery(filepath);

            return string.Format(rawQuery, parameters);
        }
    }
}
