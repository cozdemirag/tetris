﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.SCRIPTS.Utility
{
    [RequireComponent(typeof(Button))]
    public class ButtonSound : MonoBehaviour
    {
        public AudioClip Sound;

        private Button Button { get { return this.GetComponent<Button>(); } }
        private AudioSource Source { get { return this.GetComponent<AudioSource>(); } }


        public void Awake()
        {
            this.gameObject.AddComponent<AudioSource>();
            this.Source.clip = this.Sound;
            this.Source.playOnAwake = false;

            this.Button.onClick.AddListener(() =>
            {
                this.Source.PlayOneShot(this.Sound);
            });
        }
    }

}
