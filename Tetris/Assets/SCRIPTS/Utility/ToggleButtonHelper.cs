﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

namespace Assets.SCRIPTS.Utility
{
    public static class ToggleButtonHelper
    {
        public static IEnumerator<bool> SetDefaultToggleState_CoRoutine(this Toggle toggle, bool state)
        {
            var toggleEvent = toggle.onValueChanged;
            toggle.gameObject.SetActive(false);
            toggle.onValueChanged = new Toggle.ToggleEvent();

            yield return false;

            toggle.isOn = state;

            toggle.AdjustToggleLabel();

            yield return false;

            toggle.onValueChanged = toggleEvent;
            toggle.gameObject.SetActive(true);
        }

        public static void AdjustToggleLabel(this Toggle toggle)
        {
            var toggleLabel = toggle.GetComponentsInChildren<Text>().FirstOrDefault(it => it.name == "Label");

            if (toggleLabel != null)
            {
                toggleLabel.text = toggle.isOn ? "ON" : "OFF";
            }
        }
    }
}
