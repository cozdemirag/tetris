﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {
   
    public void StartP1Game()
    {
        SceneManager.LoadScene("game_p1", LoadSceneMode.Single);
    }

    public void StartP2Game()
    {
        SceneManager.LoadScene("game_p2", LoadSceneMode.Single);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("menu", LoadSceneMode.Single);
    }

    public void LoadSettings()
    {
        SceneManager.LoadScene("settings", LoadSceneMode.Single);
    }

    public void LoadAbout()
    {
        SceneManager.LoadScene("about", LoadSceneMode.Single);
    }

    public void LoadScoreboard()
    {
        SceneManager.LoadScene("scoreboard", LoadSceneMode.Single);
    }

    public void LoadP1ProfileSelection()
    {
        SceneManager.LoadScene("p1_profileSelection", LoadSceneMode.Single);
    }

    public void LoadP2ProfileSelection()
    {
        SceneManager.LoadScene("p2_profile_selection", LoadSceneMode.Single);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
