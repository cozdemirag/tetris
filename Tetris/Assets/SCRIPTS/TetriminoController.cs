﻿using Assets.SCRIPTS.Models;
using Assets.SCRIPTS.Utility;
using System;
using UnityEngine;

public class TetriminoController : MonoBehaviour
{
    public bool isRotationAllowed;
    public bool isRotationLimited;
    public int spawnX = 3;
    public int spawnY = 0;

    private Board board;
    private float fallDownTimer = 0;
    private float FallDownCooldown = 1f;
    private static float MovementCooldown = 0.10f;
    private float moveTimer = 0;
    private IControllerScheme controllerScheme;

    public EventHandler<TetriminoBecomeInActiveArgs> OnBecomingInActive;

    private bool killNextFrame = false;
    private bool isPaused = false;
    private bool IsActive = true;
    private bool CanBeControlled = false;
    private int speedMultiplier;
    private int softDropLevelsFalled = 0;

    public void Init(Board board, IControllerScheme controllerScheme, int level, float falldownCooldown)
    {
        this.board = board;
        this.controllerScheme = controllerScheme;
        this.CanBeControlled = false;
        this.speedMultiplier = 1;
        this.FallDownCooldown = falldownCooldown;
    }

    public void MakeCurrent()
    {
        this.CanBeControlled = true;
        this.transform.position = this.board.GetSpawnPosition(this.spawnX, this.spawnY);
        if (this.board.HasBecomeInActive(this.transform))
        {
            this.killNextFrame = true;
           
            return;
        }
    }

    public void ToggleBeingPaused()
    {
        this.isPaused = !this.isPaused;
    }

    // Update is called once per frame
    void Update()
    {
        if (!this.CanBeControlled || this.isPaused || this.board == null)
        {
            return;
        }

        if (killNextFrame)
        {
            this.killNextFrame = false;
            this.IsActive = false;
            this.CanBeControlled = false;
            if (this.OnBecomingInActive != null)
            {
                this.OnBecomingInActive(this, new TetriminoBecomeInActiveArgs());
            }

            return;
        }
      

        this.moveTimer += Time.deltaTime * 1;

        if (this.controllerScheme == null || !this.IsActive)
        {
            var isAllMinosDestroyed = true;
            foreach (Transform child in this.transform)
            {
                isAllMinosDestroyed &= !child.gameObject.activeSelf;
            }

            if (isAllMinosDestroyed || this.transform.childCount == 0)
            {
                DestroyObject(this.gameObject);
            }

            return;
        }

        if (this.IsActive && this.isRotationAllowed && this.controllerScheme.IsRotateKeyUp())
        {
            var angle = 90;

            if (this.isRotationLimited)
            {
                angle = this.transform.rotation.eulerAngles.z == 90 ? -90 : 90;
            }

            this.Rotate(angle);
            var originPosition = this.transform.position;
            var isRotationFailed = this.board.ShouldRevertRotation(this.transform);

            if (isRotationFailed)
            {
                this.transform.position += new Vector3(-1, 0);
                isRotationFailed = this.board.ShouldRevertRotation(this.transform);

                if (isRotationFailed)
                {
                    this.transform.position = originPosition;
                    this.transform.position += new Vector3(1, 0);

                    isRotationFailed = this.board.ShouldRevertRotation(this.transform);
                }
            }

            if (isRotationFailed)
            {
                this.Rotate(-angle);
                this.transform.position = originPosition;
            }
        }
        else if (this.moveTimer >= MovementCooldown && this.controllerScheme.IsSoftDropKeyUp())
        {
            this.moveTimer = 0;
            this.fallDownTimer = 0;

            if (this.board.HasBecomeInActive(this.transform))
            {
                this.IsActive = false;
                if (this.OnBecomingInActive != null)
                {
                    this.OnBecomingInActive(this, new TetriminoBecomeInActiveArgs { LevelsSoftDropped = this.softDropLevelsFalled });
                    this.softDropLevelsFalled = 0;
                }
                return;
            }
            else
            {
                this.softDropLevelsFalled++;
                this.transform.position += (new Vector3(0, -1, 0));
            }

            return;
        }
        else if (this.moveTimer >= MovementCooldown && this.controllerScheme.IsLeftKeyUp())
        {
            this.moveTimer = 0;
            this.transform.position += (new Vector3(-1, 0, 0));

            if (this.board.ShouldRevertMoveLeft(this.transform))
            {
                this.transform.position += (new Vector3(1, 0, 0));
            }

        }
        else if (this.moveTimer >= MovementCooldown && this.controllerScheme.IsRightKeyUp())
        {
            this.moveTimer = 0;
            this.transform.position += (new Vector3(1, 0, 0));
            if (this.board.ShouldRevertMoveRight(this.transform))
            {
                this.transform.position += (new Vector3(-1, 0, 0));
            }
        }

        if (this.IsActive && this.controllerScheme.IsDropKeyUp())
        {
            this.fallDownTimer = 0;
            var levelsToDrop = this.board.GetDropPosition(this.transform);
            this.IsActive = false;

            if (levelsToDrop > 0)
            {
                this.transform.position += (new Vector3(0, levelsToDrop * -1, 0));
                if (this.board.HasBecomeInActive(this.transform))
                {
                    if (this.OnBecomingInActive != null)
                    {
                        this.OnBecomingInActive(this, new TetriminoBecomeInActiveArgs { LevelsHardDropped = levelsToDrop });
                    }
                    return;
                }
            }
            else
            {
                this.IsActive = true;
            }
        }
        else
        {
            if (fallDownTimer >= FallDownCooldown)
            {
                this.fallDownTimer = 0;

                if (this.board.HasBecomeInActive(this.transform))
                {
                    this.IsActive = false;
                    if (this.OnBecomingInActive != null)
                    {
                        this.OnBecomingInActive(this, new TetriminoBecomeInActiveArgs { LevelsSoftDropped = this.softDropLevelsFalled });
                        this.softDropLevelsFalled = 0;
                    }
                    return;
                }
                else
                {
                    this.transform.position += (new Vector3(0, -1, 0));
                }
            }
            else
            {
                this.fallDownTimer += Time.deltaTime * (1 + (this.speedMultiplier * 0.3f));
            }
        }
    }

    private void Rotate(int angle)
    {
        this.transform.Rotate(0, 0, angle, Space.Self);

        foreach (Transform mino in this.transform)
        {
            mino.transform.Rotate(0, 0, -angle, Space.Self);
        }
    }

}
