﻿using UnityEngine;
using Assets.SCRIPTS.Utility;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GridManager : MonoBehaviour
{

    public GameObject GameOverPanel;
    public GameObject GameOverPanel_P1;
    public GameObject GameOverPanel_P2;
    public GameObject PausePanel;

    public Text WinnerNameText;
    public Text WinnerScoreText;
    public Text WinnerLinesClearedText;
    public Text WinnerLevelText;

    public Text P1_ScoreDifferenceText;
    public Text P2_ScoreDifferenceText;

    private Dictionary<PlayerScheme, GridController> grids;
    private Dictionary<PlayerScheme, GameObject> gameOverPanels;
    private Dictionary<PlayerScheme, bool> pausePanels;

    private ScoreboardRepository scoreRepository;

    private List<PlayerGridArgs> localRoundFinishedInfo;

    // Use this for initialization
    void Start()
    {
        this.localRoundFinishedInfo = new List<PlayerGridArgs>();
        this.scoreRepository = new ScoreboardRepository();
        this.grids = new Dictionary<PlayerScheme, GridController>();
        this.gameOverPanels = new Dictionary<PlayerScheme, GameObject>();
        this.pausePanels = new Dictionary<PlayerScheme, bool>();

        if (this.GameOverPanel_P1 != null)
        {
            this.gameOverPanels.Add(PlayerScheme.P1, this.GameOverPanel_P1);
            this.pausePanels.Add(PlayerScheme.P1, false);
        }
        if (this.GameOverPanel_P2 != null)
        {
            this.gameOverPanels.Add(PlayerScheme.P2, this.GameOverPanel_P2);
            this.pausePanels.Add(PlayerScheme.P2, false);
        }

        foreach (Transform obj in this.transform)
        {
            var gridController = obj.GetComponent<GridController>();

            if (gridController != null)
            {
                gridController.OnGameOver_GridEvent += GridController_OnGameOver_GridEvent_Handler;
                gridController.TogglePause_GridEvent += GridController_TogglePause_GridEvent_GridEvent_Handler;

                this.grids.Add(gridController.PlayerScheme, gridController);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void GridController_OnGameOver_GridEvent_Handler(object sender, PlayerGridArgs args)
    {
        if (this.gameOverPanels.Any())
        {
            // 2P MODE
            if (this.gameOverPanels.Any(it => it.Value.activeSelf))
            {
                foreach (var gameOverPanel in this.gameOverPanels)
                {
                    gameOverPanel.Value.SetActive(false);
                }

                this.localRoundFinishedInfo.Add(args);

                var winner = this.localRoundFinishedInfo.OrderByDescending(it => it.Score).FirstOrDefault();

                if (winner != null)
                {
                    this.scoreRepository.AddScore(winner.Score, winner.Id);

                    if (this.WinnerLevelText != null)
                    {
                        this.WinnerLevelText.text = winner.Level.ToString();
                    }

                    if (this.WinnerNameText != null)
                    {
                        this.WinnerNameText.text = winner.Player.ToString();
                    }

                    if (this.WinnerLinesClearedText != null)
                    {
                        this.WinnerLinesClearedText.text = winner.LinesCleared.ToString();
                    }

                    if (this.WinnerScoreText != null)
                    {
                        this.WinnerScoreText.text = winner.Score.ToString();
                    }

                }

                this.GameOverPanel.SetActive(true);
                EventSystem.current.SetSelectedGameObject(this.GameOverPanel.GetComponentsInChildren<Button>().FirstOrDefault().gameObject);
            }
            else
            {
                this.localRoundFinishedInfo.Add(args);
                this.gameOverPanels[args.Player].SetActive(true);
            }
        }
        else
        {
            // 1P Mode
            this.scoreRepository.AddScore(args.Score, args.Id);
            this.GameOverPanel.SetActive(true);
            EventSystem.current.SetSelectedGameObject(this.GameOverPanel.GetComponentsInChildren<Button>().FirstOrDefault().gameObject);
        }
    }

    private void GridController_TogglePause_GridEvent_GridEvent_Handler(object sender, PlayerGridArgs args)
    {
        if (this.pausePanels.Any())
        {
            if (this.pausePanels.ContainsKey(args.Player))
            {
                this.pausePanels[args.Player] = true;
            }
        }

        if (this.pausePanels.All(it => it.Value))
        {
            this.TogglePause();
        }
    }

    public void ToggleMusic()
    {
        SoundManager.GetInstance().ToggleMusic();
    }

    public void TogglePause()
    {
        this.TogglePause(null);
    }

    private void TogglePause(GridController grid)
    {
        this.PausePanel.SetActive(!this.PausePanel.activeSelf);

        if (grid == null)
        {
            foreach (GridController gridController in this.grids.Values.Where(it => it != null).ToList())
            {
                gridController.TogglePause();
            }
        }
        else
        {
            grid.TogglePause();
        }

        if (this.PausePanel.activeSelf)
        {
            EventSystem.current.SetSelectedGameObject(this.PausePanel.GetComponentsInChildren<Button>().FirstOrDefault().gameObject);
        }

        if (this.pausePanels.ContainsKey(PlayerScheme.P1))
        {
            this.pausePanels[PlayerScheme.P1] = !this.pausePanels[PlayerScheme.P1];
        }
        if (this.pausePanels.ContainsKey(PlayerScheme.P2))
        {
            this.pausePanels[PlayerScheme.P2] = !this.pausePanels[PlayerScheme.P2];
        }
    }
}
