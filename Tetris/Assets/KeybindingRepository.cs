﻿using Assets.SCRIPTS.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class KeybindingRepository
    {
        private static string SettingsFileName = "keybindings";

        public void Save(Dictionary<Controls, string> dict)
        {
            var sb = new StringBuilder();

            foreach (var pair in dict)
            {
                sb.AppendLine(pair.Key.ToString() + "=" + pair.Key);
            }

            File.WriteAllText(this.Filepath, sb.ToString());
        }

        public Dictionary<Controls, string> Get()
        {
            var result = new Dictionary<Controls, string>();

            foreach(var line in File.ReadAllLines(this.Filepath))
            {
                var pair = line.Split('=');

                var len = pair == null ? 0 : pair.Length;

                if (len != 2)
                {
                    continue;
                }

                try
                {
                    var matchedEnum = (Controls)Enum.Parse(typeof(Controls), pair[0]);

                    if (result.ContainsKey(matchedEnum))
                    {
                        result[matchedEnum] = pair[1];
                    }
                    else
                    {
                        result.Add(matchedEnum, pair[1]);
                    }
                }
                catch
                {

                }
            }

            return result;
        }

        private string Filepath { get { return Application.persistentDataPath + "/" + SettingsFileName; } }
    }
}
