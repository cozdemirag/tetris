﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
    public AudioClip BoomTetrisSfx;
    public AudioClip TripleSfx;
    public AudioClip DoubleSfx;
    public AudioClip GameOverSfx;
    public AudioClip TetriminoDropSfx;

    private AudioSource BackGroundMusicSource;

    private static SoundManager Instance;

    public static SoundManager GetInstance()
    {
        return Instance;
    }

    // Use this for initialization
    void Awake()
    {
        if (Instance == null)
        {
            var isMuted = false;
            bool.TryParse(PlayerPrefs.GetString("ToggleMusic"), out isMuted);

            this.BackGroundMusicSource = this.gameObject.GetComponent<AudioSource>();

            if (this.BackGroundMusicSource != null)
            {
                this.BackGroundMusicSource.mute = isMuted;
                this.BackGroundMusicSource.Play();
            }


            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            DestroyObject(this);
        }
    }

    public void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public float GetVolume()
    {
        return this.BackGroundMusicSource.volume;
    }

    public void SetVolume(float volume)
    {
        this.BackGroundMusicSource.volume = volume;
    }

    public void ToggleMusic()
    {
        this.BackGroundMusicSource.mute = !this.BackGroundMusicSource.mute;

        PlayerPrefs.SetString("ToggleMusic", this.BackGroundMusicSource.mute.ToString());
        PlayerPrefs.Save();
    }

    public void PlayGameOver()
    {
        AudioSource.PlayClipAtPoint(this.GameOverSfx, Vector3.zero);
    }

    public void PlayTetriminoDrop()
    {
        AudioSource.PlayClipAtPoint(this.TetriminoDropSfx, Vector3.zero);
    }

    public void PlayTetris()
    {
        AudioSource.PlayClipAtPoint(this.BoomTetrisSfx, Vector3.zero);
    }

    public void PlayRowClearSfx(int rowCount)
    {
        switch (rowCount)
        {
            case 4:
                PlayOnce(this.BoomTetrisSfx);
                break;
            case 3:
                PlayOnce(this.TripleSfx);
                break;
            case 2:
                PlayOnce(this.DoubleSfx);
                break;
            default:
                break;
        }
    }

    private void PlayOnce(AudioClip clip)
    {
        AudioSource.PlayClipAtPoint(clip, Vector3.zero);
    }

    public bool IsBackgroundMusicMuted()
    {
        return this.BackGroundMusicSource.mute;
    }
}
